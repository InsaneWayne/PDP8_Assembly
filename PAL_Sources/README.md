
 FIZBUZ.PA : Fizz Buzz (https://en.wikipedia.org/wiki/Fizz_buzz) program in PAL8
             for use with the PDP-8 PAL-D assembler.
 
 FIZBUZ.M8 : Same Fizz Buzz program with some small changes so it can be
             assembled with PDP-8 MACRO-8 assembler.
             